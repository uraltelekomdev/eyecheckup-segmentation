import cv2 as cv
import numpy as np


def overlay_image(image, bin_mask, color=(255, 0, 227)):
    temp_image = image.copy()
    color_image = np.zeros(image.shape, dtype=np.uint8)
    color_image[:] = color
    overlayed_image = cv.addWeighted(color_image, 0.3, image, 0.7, 0)
    temp_image[bin_mask] = overlayed_image[bin_mask]
    return temp_image


def get_dark_background_image(image, bin_mask):
    result_image = image.copy()
    result_image[~bin_mask] = (0, 0, 0)
    return result_image


def get_dark_feature_image(image, bin_mask):
    result_image = image.copy()
    result_image[bin_mask] = (0, 0, 0)
    return result_image
