from PyQt5 import QtWidgets
from photo_viewer import PhotoViewer
import numpy as np
import os
import cv2 as cv
from PyQt5.QtCore import Qt
from segmenter import Segmenter
from helper_image_functions import overlay_image, get_dark_feature_image, get_dark_background_image
from PyQt5.QtWidgets import QFileDialog
import pandas as pd
import globals
from segment_threading import Worker

import json

from shutil import copyfile

SINGLE = 1
MULTIPLE = 2

DARK_BACKGROUND = 1
OVERLAY = 2
DARK_FEATURE = 3

SE = 1
NON_SE = 2

JSON_FILE = "label_dict.json"


class LabellingWindow(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super(LabellingWindow, self).__init__(parent)
        self.parent = parent
        self.selection_method = SINGLE
        self.view_method = OVERLAY
        self.delete_segment = False
        self.selection_map = None
        self.img_file_list = []
        self.img_name_list = []
        self.index = -1
        self.root_folder = None
        self.label_dict = None
        self.bbox_data = None
        self.img_bbox_data = None

        self.required_folders = \
            ["segmentation"]

        self.create_widgets()

    def browse_folder(self):

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        options |= QFileDialog.DontUseCustomDirectoryIcons
        dialog = QFileDialog()
        dialog.setOptions(options)
        dialog.setFileMode(QFileDialog.Directory)
        fileRootAddress = dialog.getExistingDirectory(self, "Select Root Folder.",
                                                      options=QFileDialog.DontUseNativeDialog | QFileDialog.DontResolveSymlinks)
        if fileRootAddress:
            self.root_folder = fileRootAddress
            for file in os.listdir(fileRootAddress):
                if ((file.endswith(".jpg")) or (file.endswith(".JPG")) or (file.endswith(".jpeg")) or
                    (file.endswith(".png")) or file.endswith("tiff") or file.endswith(".PNG")) or \
                        file.endswith("JPEG"):
                    self.img_file_list.append(os.path.join(fileRootAddress, file).replace("\\", "/"))
                    self.img_name_list.append(file)

            for folder in self.required_folders:
                temp_path = os.path.join(fileRootAddress, folder)
                if not os.path.exists(temp_path):
                    os.makedirs(temp_path)

            self.arrange_label_dict()
            self.read_bbox_info()
            # while self.img_name_list[self.index + 1] in self.label_dict.keys():
            #     self.index += 1
            #     print(self.index)
            self.next_image()

    def next_image(self):
        self.index += 1
        self.index_label.setText(str(self.index + 1))
        self.current_index_label.setText("{}/{}".format(self.index + 1, len(self.img_file_list)))

        if self.index < len(self.img_file_list):
            if self.is_segment_exists(self.index):
                self.read_maps()
                self.load_image()
                if not self.is_segment_exists(self.index + 1):
                    self.start_segment_thread(self.index + 1)

            else:
                self.start_segment_thread(self.index)

    def read_maps(self):
        self.segments = np.load(os.path.join(self.root_folder,
                                             "segmentation",
                                             self.img_name_list[self.index] + "_segmentation_map.npy"))
        self.clustered_segments = np.load(os.path.join(self.root_folder,
                                                       "segmentation",
                                                       self.img_name_list[
                                                           self.index] + "_clustered_segmentation_map.npy"))
        self.cluster_centers = np.load(os.path.join(self.root_folder,
                                                    "segmentation",
                                                    self.img_name_list[self.index] + "_cluster_centers.npy"))

    def read_bbox_info(self):
        self.bbox_data = pd.read_csv("SE-BBox.csv")

    def arrange_label_dict(self):
        if os.path.exists(os.path.join(self.root_folder,
                                       "segmentation",
                                       JSON_FILE)):
            with open(os.path.join(self.root_folder,
                                       "segmentation",
                                       JSON_FILE), "r") as read_file:
                self.label_dict = json.load(read_file)

        else:
            self.label_dict = {}

    def save_labels(self):
        with open(os.path.join(self.root_folder,
                                       "segmentation",
                                       JSON_FILE), "w") as write_file:
            json.dump(self.label_dict, write_file, indent=4)
            write_file.close()

    def start_segment_thread(self, index):

        image_name = self.img_name_list[index]

        if image_name in list(globals.thread_dict.keys()):
            globals.thread_dict[index].quit()
            globals.thread_dict.pop(index)

        image_path = self.img_file_list[index]
        args = image_path, self.root_folder, image_name
        segment_thread = Worker(segment_core, image_name, index, args)
        segment_thread.finished.connect(self.segment_thread_finished_handler)
        segment_thread.error.connect(self.segment_thread_error_handler)
        globals.thread_dict[index] = segment_thread
        segment_thread.start()

    def segment_thread_finished_handler(self, index):
        print("Finished processing {}".format(index))
        globals.thread_dict.pop(index)

        if self.index == index:
            self.read_maps()
            self.load_image()

            if not self.is_segment_exists(index + 1):
                self.start_segment_thread(index + 1)

    def segment_thread_error_handler(self, index):
        print("Error while segmenting {}".format(index))
        globals.thread_dict.pop(index)

    def load_image(self):
        # print(self.img_file_list)
        # print(self.index)
        self.image = cv.imread(self.img_file_list[self.index])
        self.segment_image = self.image.copy()

        self.arrange_selection_map()

        self.arrange_bbox_info()
        self.selection_map = np.logical_and(self.selection_map, self.bbox_map)

        result_image = overlay_image(self.segment_image,
                                     self.selection_map)

        self.fundus_viewer.load_image(result_image)

        temp_map = self.bbox_map.copy()
        temp_map = temp_map.astype(np.uint8)
        temp_map *= 255

        contours, hier = cv.findContours(temp_map, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        cv.drawContours(self.image, contours, -1, (0, 255, 0), 5)

        self.original_fundus_viewer.load_image(self.image)

    def arrange_bbox_info(self):
        self.bbox_map = np.zeros(self.selection_map.shape, dtype=np.bool)
        temp_bbox_data = self.bbox_data[self.bbox_data["StoragePath"].str.contains(self.img_name_list[self.index])]
        self.img_bbox_data = temp_bbox_data[["Xmin", "Ymin", "Xmax", "Ymax"]].values.tolist()

        h, w = self.selection_map.shape
        for [xmin, ymin, xmax, ymax] in self.img_bbox_data:
            self.bbox_map[int(ymin*h):int(ymax*h), int(xmin*w):int(xmax*w)] = True

    def arrange_selection_map(self):
        image_name = self.img_name_list[self.index]
        self.selection_map = np.zeros(self.segments.shape, dtype=np.bool)
        if image_name in self.label_dict.keys():
            temp_map = np.isin(self.segments, self.label_dict[image_name]["single"])
            self.selection_map = np.logical_or(self.selection_map, temp_map)
            temp_map = np.isin(self.clustered_segments, self.label_dict[image_name]["multiple"])
            self.selection_map = np.logical_or(self.selection_map, temp_map)
        else:
            self.label_dict[image_name] = {"single": [], "multiple": []}

    def is_segment_exists(self, index):
        if self.index >= len(self.img_file_list):
            return True

        if os.path.exists(
                os.path.join(self.root_folder,
                             "segmentation",
                             self.img_name_list[index] +
                             "_segmentation_map.npy")):
            return True

        return False

    def prev_image(self):
        self.index -= 1
        if self.index > -1:
            if self.index < len(self.img_file_list):
                if self.is_segment_exists(self.index):
                    self.read_maps()
                    self.load_image()
                else:
                    self.start_segment_thread(self.index)

    def create_widgets(self):
        self.menu_Bar = QtWidgets.QMenuBar()
        self.menu_action = self.menu_Bar.addMenu("File")
        self.browse_action = QtWidgets.QAction()
        self.browse_action.setText("Browse Folder")
        self.menu_action.addAction(self.browse_action)
        self.browse_action.triggered.connect(self.browse_folder)

        self.current_index_label = QtWidgets.QLabel()
        self.current_index_label.setMaximumWidth(60)
        self.index_label = QtWidgets.QLineEdit()
        self.index_label.setMaximumWidth(60)
        self.change_index = QtWidgets.QPushButton()
        self.change_index.setMaximumWidth(120)

        self.single_select_checkbox = QtWidgets.QRadioButton()
        self.multi_select_checkbox = QtWidgets.QRadioButton()

        self.select_button_array = [self.single_select_checkbox, self.multi_select_checkbox]

        self.fundus_viewer = PhotoViewer(parent=self, drawable=True,
                                         mouse_event_func=self.mouse_event)
        self.original_fundus_viewer = PhotoViewer(parent=self, drawable=True,
                                         mouse_event_func=self.mouse_event)
        self.fundus_viewer.add_friend_viewer([self.original_fundus_viewer])
        self.original_fundus_viewer.add_friend_viewer([self.fundus_viewer])

        self.se_radiobutton = QtWidgets.QRadioButton()
        self.non_se_radiobutton = QtWidgets.QRadioButton()

        self.next_photo_button = QtWidgets.QPushButton()
        self.next_photo_button.clicked.connect(self.next_image)
        self.prev_photo_button = QtWidgets.QPushButton()

        self.dark_background_radiobutton = QtWidgets.QRadioButton()
        self.overlay_radiobutton = QtWidgets.QRadioButton()
        self.dark_feature_radiobutton = QtWidgets.QRadioButton()

        self.view_button_array = [self.dark_background_radiobutton, self.overlay_radiobutton, self.dark_feature_radiobutton]

        self.arrange_widgets()
        self.create_layouts()

    def create_layouts(self):
        self.main_layout = QtWidgets.QVBoxLayout()
        spacerItem = QtWidgets.QSpacerItem(0, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontal_layout = QtWidgets.QHBoxLayout()
        self.horizontal_layout_for_buttons = QtWidgets.QHBoxLayout()
        self.vertical_layout = QtWidgets.QVBoxLayout()
        self.vertical_layout2 = QtWidgets.QVBoxLayout()
        self.vertical_layout3 = QtWidgets.QVBoxLayout()
        self.fundus_layout = QtWidgets.QHBoxLayout()
        self.index_layout = QtWidgets.QVBoxLayout()
        self.index_h_layout = QtWidgets.QHBoxLayout()

        self.horizontal_layout_for_buttons.addWidget(self.prev_photo_button)
        self.horizontal_layout_for_buttons.addWidget(self.next_photo_button)

        self.vertical_layout.addWidget(self.single_select_checkbox)
        self.vertical_layout.addWidget(self.multi_select_checkbox)

        self.index_h_layout.addWidget(self.current_index_label)
        self.index_h_layout.addWidget(self.index_label)
        self.index_h_layout.addWidget(self.change_index)
        self.index_layout.addLayout(self.index_h_layout)
        self.index_layout.addLayout(self.horizontal_layout_for_buttons)

        self.vertical_layout2.addWidget(self.se_radiobutton)
        self.vertical_layout2.addWidget(self.non_se_radiobutton)

        self.vertical_layout3.addWidget(self.dark_background_radiobutton)
        self.vertical_layout3.addWidget(self.overlay_radiobutton)
        self.vertical_layout3.addWidget(self.dark_feature_radiobutton)

        self.horizontal_layout.addLayout(self.index_layout)
        self.horizontal_layout.addLayout(self.vertical_layout)
        # self.horizontal_layout.addLayout(self.index_layout)
        self.horizontal_layout.addLayout(self.vertical_layout3)

        self.fundus_layout.addWidget(self.original_fundus_viewer)
        self.fundus_layout.addWidget(self.fundus_viewer)

        self.main_layout.addWidget(self.menu_Bar)
        self.main_layout.addItem(self.horizontal_layout)
        self.main_layout.addItem(self.fundus_layout)

        self.setLayout(self.main_layout)

    def arrange_widgets(self):
        self.next_photo_button.setText("Next")
        self.prev_photo_button.setText("Prev")

        self.change_index.setText("Go to Index")

        self.single_select_checkbox.setText("Single Select")
        self.multi_select_checkbox.setText("Multiple Select")

        self.se_radiobutton.setText("SE")
        self.non_se_radiobutton.setText("NON_SE")

        self.dark_background_radiobutton.setText("Dark Background")
        self.overlay_radiobutton.setText("Overlay")
        self.dark_feature_radiobutton.setText("Dark Feature")

        self.single_select_checkbox.setChecked(True)

        self.current_index_label.setText("0/0")
        self.index_label.setText("-")

        self.single_select_checkbox.clicked.connect(
             lambda: self.change_selection_method(SINGLE))
        self.multi_select_checkbox.clicked.connect(
            lambda: self.change_selection_method(MULTIPLE))

        self.dark_background_radiobutton.clicked.connect(
            lambda: self.change_view_method(DARK_BACKGROUND))
        self.overlay_radiobutton.clicked.connect(
            lambda: self.change_view_method(OVERLAY))
        self.dark_feature_radiobutton.clicked.connect(
            lambda: self.change_view_method(DARK_FEATURE))

        self.change_index.clicked.connect(self.go_to_index)

    def go_to_index(self):
        index = int(self.index_label.text())

        if index > 0 and index <= len(self.img_file_list):
            self.index = index - 2
            self.next_image()

    def change_selection_method(self, method):
        self.selection_method = method
        # self.arrange_radio_buttons()

    def change_view_method(self, method):
        self.view_method = method
        # self.arrange_radio_buttons()
        self.arrange_view()

    def arrange_radio_buttons(self):
        self.select_button_array[self.selection_method - 1].setChecked(True)
        self.view_button_array[self.view_method - 1].setChecked(True)

    def arrange_view(self):
        if self.view_method == DARK_BACKGROUND:
            result_image = get_dark_background_image(self.segment_image,
                                                     self.selection_map)

        elif self.view_method == OVERLAY:
            result_image = overlay_image(self.segment_image,
                                         self.selection_map)
        else:
            result_image = get_dark_feature_image(self.segment_image,
                                                  self.selection_map)

        self.fundus_viewer.set_image(result_image)

    def mouse_event(self, event):
        column = int(event.scenePos().x())
        row = int(event.scenePos().y())

        if not self.bbox_map[row, column]:
            return

        key = "multiple"
        if self.selection_method == SINGLE:
            key = "single"

        if event.button() == Qt.LeftButton:

            map_pointer = self.clustered_segments
            if self.selection_method == SINGLE:
                map_pointer = self.segments

            label = map_pointer[row, column]
            temp_map = map_pointer == label

            self.selection_map = np.logical_or(self.selection_map, temp_map)
            self.selection_map = np.logical_and(self.selection_map, self.bbox_map)

            self.arrange_view()

            if int(label) not in self.label_dict[self.img_name_list[self.index]][key]:
                self.label_dict[self.img_name_list[self.index]][key].append(int(label))

        if event.button() == Qt.RightButton:
            map_pointer = self.clustered_segments
            if self.selection_method == SINGLE:
                map_pointer = self.segments

            label = map_pointer[row, column]
            temp_map = map_pointer == label

            self.selection_map = np.logical_and(self.selection_map, ~temp_map)
            self.selection_map = np.logical_and(self.selection_map, self.bbox_map)

            self.arrange_view()

            try:
                self.label_dict[self.img_name_list[self.index]][key].remove(int(label))
            except:
                pass
        self.save_labels()


def segment_core(args):
    image_path, root_folder, image_name = args[0]

    segmenter_obj = Segmenter(image_path)
    segmenter_obj.get_segment()

    segments, clustered_segments, cluster_centers = \
        segmenter_obj.over_segmented, \
        segmenter_obj.clustered_segments, segmenter_obj.cluster_centers
    np.save(os.path.join(root_folder,
                         "segmentation",
                         image_name + "_segmentation_map.npy"),
            segments)
    np.save(os.path.join(root_folder,
                         "segmentation",
                         image_name + "_clustered_segmentation_map.npy"),
            clustered_segments)
    np.save(os.path.join(root_folder,
                         "segmentation",
                         image_name + "_cluster_centers.npy"),
            cluster_centers)
