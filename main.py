from PyQt5 import QtWidgets
from labelling_window import LabellingWindow


if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)

    window = LabellingWindow()
    window.showMaximized()
    res = app.exec_()
    sys.exit(res)