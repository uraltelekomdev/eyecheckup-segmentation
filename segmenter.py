from skimage.segmentation import mark_boundaries, felzenszwalb
from skimage.color import rgb2lab, rgb2hsv
import numpy as np
import cv2 as cv
from math import sqrt, pow
from skimage.exposure import histogram
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import os

L_index, A_index, B_index = 0, 1, 2
L_min, L_max, A_min, A_max, B_min, B_max = 0, 100, -128, 127, -128, 127
# L_min, L_max, A_min, A_max, B_min, B_max = 0, 255, 0, 255, 0, 255


"""
Creates 3 results according to arguments "seg_type" and "cluster" in get_segments() function
1) Initial over segmented result - self.over_segmented
3) Clustered clustered_segments  - self.clustered_segments
    3.a) cluster centers - self.cluster_centers
"""


class Segmenter:

    def __init__(self, image_path):
        # self.start = time.clock()
        self.image_path = image_path
        self.image = cv.cvtColor(cv.imread(image_path), cv.COLOR_BGR2RGB)
        self.clustered_segments = None
        self.cluster_centers = None
        self.merged_segments = None
        self.over_segmented = None
        self.marked = None
        self.seg_res = None
        self.means = None

    def get_segment(self):
        self.over_segment()

        self.seg_res = self.over_segmented
        self.over_segmented = self.seg_res
        self.means = self.get_intensity_stats(self.seg_res)

        self.cluster_segments()
        self.seg_res = self.clustered_segments

    def cluster_segments(self):
        # criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        # ret, label, center = cv.kmeans(self.means, 80, None, criteria, 10, cv.KMEANS_RANDOM_CENTERS)
        # print("started")
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        flags = cv.KMEANS_RANDOM_CENTERS
        # Apply KMeans
        self.means = np.array(self.means, dtype=np.float32)
        compactness, labels, self.cluster_centers = cv.kmeans(self.means, 100, None, criteria, 10, flags)

        # model = KMeans(n_clusters=100, n_jobs=5)
        # labels = model.fit_predict(self.means)
        # self.cluster_centers = model.cluster_centers_

        self.clustered_segments = np.empty(self.seg_res.shape, dtype=
                                           np.uint8)

        for i in range(len(self.cluster_centers)):
            type_indices = np.where(labels == i)
            self.clustered_segments[np.isin(self.seg_res, type_indices)] = i

    def get_stat(self, _segment):
        _L, _A, _B = _segment[L_index], _segment[A_index], _segment[B_index]

        return (np.mean(_L), np.mean(_A), np.mean(_B)),\
               (np.add(np.histogram(_L, bins=16, range=(L_min, L_max), density=True), 1e-7)[0],
                np.add(np.histogram(_A, bins=16, range=(A_min, A_max), density=True), 1e-7)[0],
                np.add(np.histogram(_B, bins=16, range=(B_min, B_max), density=True), 1e-7)[0])

    def get_intensity_stats(self, seg_res):
        lab_image = rgb2lab(self.image)

        max_seg, min_seg = seg_res.max(), seg_res.min()

        self.segment_pixel_values = [[[], [], []] for i in range(max_seg+1)]

        for i in range(seg_res.shape[0]):
            for j in range(seg_res.shape[1]):
                seg_index = seg_res[i][j]
                l, a, b = lab_image[i][j]
                temp = self.segment_pixel_values[seg_index]
                temp[L_index].append(l)
                temp[A_index].append(a)
                temp[B_index].append(b)

        self.segment_pixel_values = np.array(self.segment_pixel_values)
        means = []

        #TODO enumarate and create array şn advance instead of append
        for _segment in self.segment_pixel_values:
            _mean, _hist = self.get_stat(_segment)
            means.append(np.concatenate((_hist[0], _hist[1], _hist[2], _mean)))

        return means

    def over_segment(self):
        min_size = 3

        self.over_segmented = felzenszwalb(self.image, scale=20, sigma=0, min_size=min_size)
