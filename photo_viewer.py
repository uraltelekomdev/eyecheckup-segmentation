from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap, QImage
import time
from config import DRAG_TIME


class LabellingScene(QtWidgets.QGraphicsScene):
    def __init__(self, mouse_event_func=None, mouse_drag_event_func=None,
                 mouse_move_func=None, parent=None):
        QtWidgets.QGraphicsScene.__init__(self, parent)
        self.parent = parent

        self.click_time = None
        self.clicked = False

        self.mouse_drag_event_func = None
        self.mouse_event_func = None
        self.mouse_move_func = None

        if mouse_drag_event_func:
            self.mouse_drag_event_func = mouse_drag_event_func
        if mouse_event_func:
            self.mouse_event_func = mouse_event_func
        if mouse_move_func:
            self.mouse_move_func = mouse_move_func

    def mouseReleaseEvent(self, event):
        if self.mouse_event_func:
            self.mouse_event_func(event)


class GraphicsScene(QtWidgets.QGraphicsScene):
    def __init__(self, mouse_press_func=None, mouse_event_func=None, mouse_drag_event_func=None,
                 mouse_move_func=None, parent=None):
        QtWidgets.QGraphicsScene.__init__(self, parent)
        self.parent = parent

        self.click_time = None
        self.clicked = False

        self.mouse_drag_event_func = None
        self.mouse_event_func = None
        self.mouse_move_func = None
        self.mouse_press_func = None

        if mouse_drag_event_func:
            self.mouse_drag_event_func = mouse_drag_event_func
        if mouse_event_func:
            self.mouse_event_func = mouse_event_func
        if mouse_move_func:
            self.mouse_move_func = mouse_move_func
        if mouse_press_func:
            self.mouse_press_func = mouse_press_func

    def mousePressEvent(self, event):
        self.click_time = time.time()
        self.clicked = True
        if self.mouse_press_func:
            self.mouse_press_func(event)

    def mouseReleaseEvent(self, event):
        if time.time() - self.click_time < DRAG_TIME and self.mouse_event_func:
            self.mouse_event_func(event)
        elif self.mouse_drag_event_func:
            self.mouse_drag_event_func(event)
        self.clicked = False

    def mouseMoveEvent(self, event):
        if self.mouse_move_func and self.clicked:
            self.mouse_move_func(event)

    def set_mouse_events(self, mouse_press_func=None,
                         mouse_event_func=None,
                         mouse_drag_event_func=None,
                         mouse_move_func=None):
        self.mouse_press_func = mouse_press_func
        self.mouse_event_func = mouse_event_func
        self.mouse_drag_event_func = mouse_drag_event_func
        self.mouse_move_func = mouse_move_func


class PhotoViewer(QtWidgets.QGraphicsView):
    photoClicked = QtCore.pyqtSignal(QtCore.QPoint)
    wheel_event_signal = QtCore.pyqtSignal(float)

    def __init__(self, parent, drawable=False, labelling=False,
                 mouse_press_func=None,
                 mouse_event_func=None,
                 mouse_drag_event_func=None,
                 mouse_move_func=None):
        super(PhotoViewer, self).__init__(parent)
        self._zoom = 0
        self._empty = True
        self.polygon_points = []
        self.friend_viewers = None

        if labelling:
            self._scene = LabellingScene(mouse_event_func=mouse_event_func,
                                         mouse_drag_event_func= mouse_drag_event_func,
                                         mouse_move_func=mouse_move_func,
                                         parent=self)
            self.setMouseTracking(True)
        elif drawable:
            self._scene = GraphicsScene(mouse_event_func=mouse_event_func,
                                        mouse_press_func=mouse_press_func,
                                        mouse_drag_event_func=mouse_drag_event_func,
                                        mouse_move_func=mouse_move_func, parent=self)
            self.setMouseTracking(True)
        else:
            self._scene = QtWidgets.QGraphicsScene()

        self._photo = QtWidgets.QGraphicsPixmapItem()

        self.image = None
        self._scene.addItem(self._photo)
        self.setScene(self._scene)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setBackgroundBrush(QtGui.QBrush(QtGui.QColor(30, 30, 30)))
        self.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)

    def hasPhoto(self):
        return not self._empty

    def fitInView(self, scale=True):
        rect = QtCore.QRectF(self._photo.pixmap().rect())
        if not rect.isNull():
            self.setSceneRect(rect)
            if self.hasPhoto():
                unity = self.transform().mapRect(QtCore.QRectF(0, 0, 1, 1))
                self.scale(1 / unity.width(), 1 / unity.height())
                viewrect = self.viewport().rect()
                scenerect = self.transform().mapRect(rect)
                factor = min(viewrect.width() / scenerect.width(),
                             viewrect.height() / scenerect.height())
                self.scale(factor, factor)
            # self._zoom = 0

    def setPhoto(self, pixmap=None):
        # self._zoom = 0
        if pixmap and not pixmap.isNull():
            self._empty = False
            # self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
            self._photo.setPixmap(pixmap)
        else:
            self._empty = True
            # self.setDragMode(QtWidgets.QGraphicsView.NoDrag)
            self._photo.setPixmap(QtGui.QPixmap())

    def change_drag_mode(self, enable):
        if not enable:
            self.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
        else:
            self.setDragMode(QtWidgets.QGraphicsView.NoDrag)

    def wheelEvent(self, event):
        if self.hasPhoto():
            if event.angleDelta().y() > 0:
                factor = 1.25
                self._zoom += 1
            else:
                factor = 0.8
                self._zoom -= 1

            if self.friend_viewers:
                for viewer in self.friend_viewers:
                    viewer._zoom = self._zoom

            if self._zoom > 0:
                self.scale(factor, factor)
                if self.friend_viewers:
                    for viewer in self.friend_viewers:
                        viewer.scale(factor, factor)
                # self.wheel_event_signal.emit(factor)
            elif self._zoom == 0:
                self.fitInView()
                if self.friend_viewers:
                    for viewer in self.friend_viewers:
                        viewer.fitInView()
            else:
                self._zoom = 0
                if self.friend_viewers:
                    for viewer in self.friend_viewers:
                        viewer._zoom = 0

    def get_image(self):
        return self.image

    # def mousePressEvent(self, event):
    #     if self._photo.isUnderMouse():
    #         self.photoClicked.emit(self.mapToScene(event.pos()).toPoint())
    #     super(PhotoViewer, self).mousePressEvent(event)

    def get_pixmap_from_image(self, image):
        temp_image = QImage(image.data, image.shape[1], image.shape[0],
                      image.shape[1] * 3, QImage.Format_BGR888)
        return QPixmap.fromImage(temp_image)

    def set_image(self, image):
        pixmap = self.get_pixmap_from_image(image)
        self.setPhoto(pixmap)

    def load_image(self, image):
        self.polygon_points = []
        self.image = image.copy()
        self.set_image(self.image)
        self.fitInView()

    def scale_both_images(self, factor):
        self.scale(factor, factor)

    def add_friend_viewer(self, viewers):
        self.friend_viewers = viewers