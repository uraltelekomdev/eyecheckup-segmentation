from PyQt5.Qt import QThread
from PyQt5 import QtCore


class Worker(QThread):

    finished = QtCore.pyqtSignal(int)
    error = QtCore.pyqtSignal(int)

    def __init__(self, func, name, index, *args):
        super(Worker, self).__init__()
        self.func = func
        self.args = args
        self.name = name
        self.index = index

    def run(self):
        print(self.name + " started")
        try:
            self.func(self.args)
            self.finished.emit(self.index)
        except:
            self.error.emit(self.index)